package com.bigcompany.service;

import com.bigcompany.common.AbstractNotificationTest;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class OrganizationAnalysisServiceTest extends AbstractNotificationTest {

    @Test
    public void shouldProperlyHandleSalaryAndReportingLineCases() {
        //given
        OrganizationAnalysisService service = new OrganizationAnalysisService(this.notificationService);
        String orgFilePath = getAbsolutePathFor("service/org_analysis_service_test_happy_path.csv");

        //when
        service.analyseOrganization(orgFilePath);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return exactly 12 messages", 12, outputLines.length);
        assertEquals("Bob Johnson earns less than they should by: 10000.00", outputLines[0]);
        assertEquals("Sarah Jane Smith earns more than they should by: 10250.00", outputLines[1]);
        assertEquals("Alice Williams earns less than they should by: 4500.00", outputLines[2]);
        assertEquals("Rob Thomas earns less than they should by: 1000.00", outputLines[3]);
        assertEquals("Danny Pink earns less than they should by: 3800.00", outputLines[4]);
        assertEquals("Jackie Tyler has a reporting line too long by 1", outputLines[5]);
        assertEquals("Martha Jones has a reporting line too long by 1", outputLines[6]);
        assertEquals("Jack Harkness has a reporting line too long by 2", outputLines[7]);
        assertEquals("Donna Noble has a reporting line too long by 2", outputLines[8]);
        assertEquals("Rose Tyler has a reporting line too long by 2", outputLines[9]);
        assertEquals("Amelia Pond has a reporting line too long by 1", outputLines[10]);
        assertEquals("Rory Williams has a reporting line too long by 1", outputLines[11]);
    }

    @Test
    public void shouldProperlyHandleCSVFileWithTooManyLines() {
        //given
        OrganizationAnalysisService service = new OrganizationAnalysisService(this.notificationService);
        String orgFilePath = getAbsolutePathFor("service/org_analysis_service_test_more_than_1000_lines.csv");

        //when
        service.analyseOrganization(orgFilePath);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return exactly 1 message", 1, outputLines.length);
        assertEquals("Provided CSV file contains too many lines: [1201] while allowed only: [1000]", outputLines[0]);
    }

    @Test
    public void shouldProperlyHandleEmptyCSVFile() {
        //given
        OrganizationAnalysisService service = new OrganizationAnalysisService(this.notificationService);
        String orgFilePath = getAbsolutePathFor("service/org_analysis_service_test_empty_file.csv");

        //when
        service.analyseOrganization(orgFilePath);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return exactly 1 message", 1, outputLines.length);
        assertEquals("Provided CSV file is empty", outputLines[0]);

    }

    private String getAbsolutePathFor(String path) {
        return new File(getClass().getClassLoader().getResource(path).getFile()).getAbsolutePath();
    }
}
