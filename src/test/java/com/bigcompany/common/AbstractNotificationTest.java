package com.bigcompany.common;

import com.bigcompany.service.NotificationService;
import org.junit.Before;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public abstract class AbstractNotificationTest {

    protected final ByteArrayOutputStream notifications = new ByteArrayOutputStream();
    protected NotificationService notificationService;

    @Before
    public void setUp() {
        this.notificationService = new NotificationService(new PrintStream(notifications));
    }
}
