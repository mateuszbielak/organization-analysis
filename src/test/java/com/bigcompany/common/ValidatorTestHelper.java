package com.bigcompany.common;

import com.bigcompany.model.Employee;
import com.bigcompany.repository.OrgStructureRepository;

public class ValidatorTestHelper {
    public static OrgStructureRepository createRepositoryWithNoCycles() {
        OrgStructureRepository repository = new OrgStructureRepository();
        Employee john = new Employee("1", "John", "Doe", 10000L, null);
        Employee ben = new Employee("2", "Ben", "Smith", 10000L, "1");
        Employee rob = new Employee("3", "Rob", "Novak", 10000L, "2");
        Employee karen = new Employee("4", "Karen", "Lamb", 10000L, "3");
        Employee alena = new Employee("5", "Alena", "White", 10000L, "3");
        Employee dana = new Employee("6", "Dana", "Diesel", 10000L, "3");
        repository.addEmployee(john);
        repository.addEmployee(ben);
        repository.addEmployee(rob);
        repository.addEmployee(karen);
        repository.addEmployee(alena);
        repository.addEmployee(dana);
        return repository;
    }

    public static OrgStructureRepository createRepositoryWithCycle() {
        OrgStructureRepository repository = new OrgStructureRepository();
        Employee john = new Employee("1", "John", "Doe", 10000L, null);
        Employee ben = new Employee("2", "Ben", "Smith", 10000L, "6");
        Employee rob = new Employee("3", "Rob", "Novak", 10000L, "2");
        Employee karen = new Employee("4", "Karen", "Lamb", 10000L, "3");
        Employee alena = new Employee("5", "Alena", "White", 10000L, "3");
        Employee dana = new Employee("6", "Dana", "Diesel", 10000L, "3");
        repository.addEmployee(john);
        repository.addEmployee(ben);
        repository.addEmployee(rob);
        repository.addEmployee(karen);
        repository.addEmployee(alena);
        repository.addEmployee(dana);
        return repository;
    }

    public static OrgStructureRepository createRepositoryCorrectManagerIds() {
        OrgStructureRepository repository = new OrgStructureRepository();
        Employee john = new Employee("1", "John", "Doe", 10000L, null);
        Employee ben = new Employee("2", "Ben", "Smith", 10000L, "1");
        Employee rob = new Employee("3", "Rob", "Novak", 10000L, "2");
        Employee karen = new Employee("4", "Karen", "Lamb", 10000L, "3");
        Employee alena = new Employee("5", "Alena", "White", 10000L, "3");
        Employee dana = new Employee("6", "Dana", "Diesel", 10000L, "3");
        repository.addEmployee(john);
        repository.addEmployee(ben);
        repository.addEmployee(rob);
        repository.addEmployee(karen);
        repository.addEmployee(alena);
        repository.addEmployee(dana);
        return repository;
    }

    public static OrgStructureRepository createRepositoryNotExistingManagerId() {
        OrgStructureRepository repository = new OrgStructureRepository();
        Employee john = new Employee("1", "John", "Doe", 10000L, null);
        Employee ben = new Employee("2", "Ben", "Smith", 10000L, "1");
        Employee rob = new Employee("3", "Rob", "Novak", 10000L, "2");
        Employee karen = new Employee("4", "Karen", "Lamb", 10000L, "300");
        Employee alena = new Employee("5", "Alena", "White", 10000L, "3");
        Employee dana = new Employee("6", "Dana", "Diesel", 10000L, "3");
        repository.addEmployee(john);
        repository.addEmployee(ben);
        repository.addEmployee(rob);
        repository.addEmployee(karen);
        repository.addEmployee(alena);
        repository.addEmployee(dana);
        return repository;
    }

    public static OrgStructureRepository createRepositoryWithNoRootNode() {
        OrgStructureRepository repository = new OrgStructureRepository();
        Employee john = new Employee("1", "John", "Doe", 10000L, "3");
        Employee ben = new Employee("2", "Ben", "Smith", 10000L, "1");
        Employee rob = new Employee("3", "Rob", "Novak", 10000L, "2");
        Employee karen = new Employee("4", "Karen", "Lamb", 10000L, "3");
        Employee alena = new Employee("5", "Alena", "White", 10000L, "3");
        Employee dana = new Employee("6", "Dana", "Diesel", 10000L, "3");
        repository.addEmployee(john);
        repository.addEmployee(ben);
        repository.addEmployee(rob);
        repository.addEmployee(karen);
        repository.addEmployee(alena);
        repository.addEmployee(dana);
        return repository;
    }

    public static OrgStructureRepository createRepositoryWithOneRootNode() {
        OrgStructureRepository repository = new OrgStructureRepository();
        Employee john = new Employee("1", "John", "Doe", 10000L, null);
        Employee ben = new Employee("2", "Ben", "Smith", 10000L, "1");
        Employee rob = new Employee("3", "Rob", "Novak", 10000L, "2");
        Employee karen = new Employee("4", "Karen", "Lamb", 10000L, "3");
        Employee alena = new Employee("5", "Alena", "White", 10000L, "3");
        Employee dana = new Employee("6", "Dana", "Diesel", 10000L, "3");
        repository.addEmployee(john);
        repository.addEmployee(ben);
        repository.addEmployee(rob);
        repository.addEmployee(karen);
        repository.addEmployee(alena);
        repository.addEmployee(dana);
        return repository;
    }

    public static OrgStructureRepository createRepositoryWithMultipleRootNodes() {
        OrgStructureRepository repository = new OrgStructureRepository();
        Employee john = new Employee("1", "John", "Doe", 10000L, null);
        Employee ben = new Employee("2", "Ben", "Smith", 10000L, "1");
        Employee rob = new Employee("3", "Rob", "Novak", 10000L, "6");
        Employee karen = new Employee("4", "Karen", "Lamb", 10000L, null);
        Employee alena = new Employee("5", "Alena", "White", 10000L, "3");
        Employee dana = new Employee("6", "Dana", "Diesel", 10000L, "3");
        repository.addEmployee(john);
        repository.addEmployee(ben);
        repository.addEmployee(rob);
        repository.addEmployee(karen);
        repository.addEmployee(alena);
        repository.addEmployee(dana);
        return repository;
    }
}
