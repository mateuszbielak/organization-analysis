package com.bigcompany;

import com.bigcompany.analyser.ReportingLinesAnalyserTest;
import com.bigcompany.analyser.SalaryAbstractAnalyserTest;
import com.bigcompany.repository.OrgStructureRepositoryTest;
import com.bigcompany.repository.OrgStructureRetrieverTest;
import com.bigcompany.repository.validator.CyclesValidatorTest;
import com.bigcompany.repository.validator.ManagerIdValidatorTest;
import com.bigcompany.repository.validator.RootNodeValidatorTest;
import com.bigcompany.service.OrganizationAnalysisServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ReportingLinesAnalyserTest.class,
        SalaryAbstractAnalyserTest.class,
        OrgStructureRepositoryTest.class,
        OrgStructureRetrieverTest.class,
        OrganizationAnalysisServiceTest.class,
        ManagerIdValidatorTest.class,
        RootNodeValidatorTest.class,
        CyclesValidatorTest.class
})
public class UnitTestsSuite {
}
