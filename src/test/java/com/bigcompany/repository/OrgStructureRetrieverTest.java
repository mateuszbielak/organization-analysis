package com.bigcompany.repository;

import com.bigcompany.exception.DataRetrievalException;
import com.bigcompany.model.Employee;
import org.junit.Test;

import java.io.File;

import static com.bigcompany.exception.DataRetrievalException.ErrorCode.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class OrgStructureRetrieverTest {

    @Test
    public void shouldProperlyHandleEmptyFile() {
        //given
        OrgStructureRetriever retriever = new OrgStructureRetriever();
        String orgFilePath = getAbsolutePathFor("repository/org_retriever_test_empty_file.csv");

        //when
        try {
            retriever.retrieveFrom(orgFilePath);
            fail("Expected exception to be thrown in case of empty file");
            ;
        } catch (DataRetrievalException e) {
            //pass
            assertEquals("EMPTY_CSV_FILE error code should be thrown", EMPTY_CSV_FILE, e.getErrorCode());
        }
    }

    @Test
    public void shouldProperlyHandleToBigFile() {
        //given
        OrgStructureRetriever retriever = new OrgStructureRetriever();
        String orgFilePath = getAbsolutePathFor("repository/org_retriever_test_to_big_file.csv");

        //when
        try {
            retriever.retrieveFrom(orgFilePath);
            fail("Expected exception to be thrown in case of too big file");
            ;
        } catch (DataRetrievalException e) {
            //pass
            assertEquals("TO_BIG_CSV_FILE error code should be thrown", TO_BIG_CSV_FILE, e.getErrorCode());
        }
    }

    @Test
    public void shouldProperlyHandleCaseOfWrongFilePath() {
        //given
        OrgStructureRetriever retriever = new OrgStructureRetriever();
        String orgFilePath = "file_path_which_do_not_exist.csv";

        //when
        try {
            retriever.retrieveFrom(orgFilePath);
            fail("Expected exception to be thrown in case of wrong file path");
            ;
        } catch (DataRetrievalException e) {
            //pass
            assertEquals("FILE_OPEN_FAILED error code should be thrown", FILE_OPEN_FAILED, e.getErrorCode());
        }
    }

    @Test
    public void shouldProperlyHandleCaseOfTooManyColumnCount() {
        //given
        OrgStructureRetriever retriever = new OrgStructureRetriever();
        String orgFilePath = getAbsolutePathFor("repository/org_retriever_test_incorrect_column_count.csv");

        //when
        try {
            retriever.retrieveFrom(orgFilePath);
            fail("Expected exception to be thrown in case of incorrect column count");
        } catch (DataRetrievalException e) {
            //pass
            assertEquals("UNEXPECTED_COLUMN_COUNT error code should be thrown", UNEXPECTED_COLUMN_COUNT, e.getErrorCode());
        }
    }

    @Test
    public void shouldProperlyHandleCaseOfIncorrectColumnCount() {
        //given
        OrgStructureRetriever retriever = new OrgStructureRetriever();
        String orgFilePath = getAbsolutePathFor("repository/org_retriever_test_not_enough_column_count.csv");

        //when
        try {
            retriever.retrieveFrom(orgFilePath);
            fail("Expected exception to be thrown in case of incorrect column count");
        } catch (DataRetrievalException e) {
            //pass
            assertEquals("UNEXPECTED_COLUMN_COUNT error code should be thrown", UNEXPECTED_COLUMN_COUNT, e.getErrorCode());
        }
    }

    @Test
    public void shouldProperlyHandleCaseOfInvalidSalaryFormat() {
        //given
        OrgStructureRetriever retriever = new OrgStructureRetriever();
        String orgFilePath = getAbsolutePathFor("repository/org_retriever_test_invalid_salary_format.csv");

        //when
        try {
            retriever.retrieveFrom(orgFilePath);
            fail("Expected exception to be thrown in case of invalid column format");
        } catch (DataRetrievalException e) {
            //pass
            assertEquals("INVALID_COLUMN_FORMAT error code should be thrown", INVALID_COLUMN_FORMAT, e.getErrorCode());
        }
    }

    @Test
    public void shouldProperlyHandleCaseOfEmptyField() {
        //given
        OrgStructureRetriever retriever = new OrgStructureRetriever();
        String orgFilePath = getAbsolutePathFor("repository/org_retriever_test_empty_field.csv");

        //when
        try {
            retriever.retrieveFrom(orgFilePath);
            fail("Expected exception to be thrown in case of invalid column format");
        } catch (DataRetrievalException e) {
            //pass
            assertEquals("INVALID_COLUMN_FORMAT error code should be thrown", INVALID_COLUMN_FORMAT, e.getErrorCode());
        }
    }

    @Test
    public void shouldProperlyRetrieveOrgStructureFromGivenFile() {
        //given
        OrgStructureRetriever retriever = new OrgStructureRetriever();
        String orgFilePath = getAbsolutePathFor("repository/org_retriever_test.csv");

        //when
        OrgStructureRepository repository = retriever.retrieveFrom(orgFilePath);

        //then
        assertEquals("", 5, repository.getAllEmployees().size());
        Employee alice = new Employee("300", "Alice", "Hasacat", 50000L, "124");
        assertEquals("", alice, repository.getEmployeeBy("300"));
        Employee joe = new Employee("123", "Joe", "Doe", 60000L, null);
        assertEquals("", joe, repository.getEmployeeBy("123"));
    }

    private String getAbsolutePathFor(String path) {
        return new File(getClass().getClassLoader().getResource(path).getFile()).getAbsolutePath();
    }

}
