package com.bigcompany.repository.validator;

import com.bigcompany.exception.DataValidationException;
import com.bigcompany.repository.OrgStructureRepository;
import org.junit.Test;

import static com.bigcompany.common.ValidatorTestHelper.createRepositoryCorrectManagerIds;
import static com.bigcompany.common.ValidatorTestHelper.createRepositoryNotExistingManagerId;
import static com.bigcompany.exception.DataValidationException.ErrorCode.NOT_EXISTING_MANAGER_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ManagerIdValidatorTest {

    @Test
    public void shouldProperlyHandleCaseWithExistingManagerIds() {
        //given
        ManagerIdValidator validator = new ManagerIdValidator();
        OrgStructureRepository repository = createRepositoryCorrectManagerIds();

        //when
        try {
            validator.validate(repository);
            //pass
        } catch (DataValidationException e) {
            fail("Should not throw any exception if all manager ids are correct");
        }
    }

    @Test
    public void shouldProperlyHandleCaseWithNotExistingManagerIds() {
        //given
        ManagerIdValidator validator = new ManagerIdValidator();
        OrgStructureRepository repository = createRepositoryNotExistingManagerId();

        //when
        try {
            validator.validate(repository);
            fail("Should throw exception if there is not existing manager id in org structure");
        } catch (DataValidationException e) {
            //pass
            assertEquals("Exception with following errorCode: NOT_EXISTING_MANAGER_ID should be thrown",
                    NOT_EXISTING_MANAGER_ID, e.getErrorCode());
        }
    }
}
