package com.bigcompany.repository.validator;

import com.bigcompany.exception.DataValidationException;
import com.bigcompany.repository.OrgStructureRepository;
import org.junit.Test;

import static com.bigcompany.common.ValidatorTestHelper.*;
import static com.bigcompany.exception.DataValidationException.ErrorCode.INVALID_ROOT_NODE_COUNT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RootNodeValidatorTest {

    @Test
    public void shouldProperlyHandleCaseWithOneRootNode() {
        //given
        RootNodeValidator validator = new RootNodeValidator();
        OrgStructureRepository repository = createRepositoryWithOneRootNode();

        //when
        try {
            validator.validate(repository);
            //pass
        } catch (DataValidationException e) {
            fail("Should not throw any exception in cae of org structure contains one root node");
        }
    }

    @Test
    public void shouldProperlyHandleCaseWithMoreThanOneRootNode() {
        //given
        RootNodeValidator validator = new RootNodeValidator();
        OrgStructureRepository repository = createRepositoryWithMultipleRootNodes();

        //when
        try {
            validator.validate(repository);
            fail("Should throw exception in cae of org structure contains more than one root node");
        } catch (DataValidationException e) {
            //pass
            assertEquals("Exception with following errorCode: INVALID_ROOT_NODE_COUNT should be thrown",
                    INVALID_ROOT_NODE_COUNT, e.getErrorCode());
        }
    }

    @Test
    public void shouldProperlyHandleCaseWithNoRootNode() {
        //given
        RootNodeValidator validator = new RootNodeValidator();
        OrgStructureRepository repository = createRepositoryWithNoRootNode();

        //when
        try {
            validator.validate(repository);
            fail("Should throw exception in cae of org structure contains no root node");
        } catch (DataValidationException e) {
            //pass
            assertEquals("Exception with following errorCode: INVALID_ROOT_NODE_COUNT should be thrown",
                    INVALID_ROOT_NODE_COUNT, e.getErrorCode());
        }
    }
}
