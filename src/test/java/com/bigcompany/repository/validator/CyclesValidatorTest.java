package com.bigcompany.repository.validator;

import com.bigcompany.exception.DataValidationException;
import com.bigcompany.repository.OrgStructureRepository;
import org.junit.Test;

import static com.bigcompany.common.ValidatorTestHelper.createRepositoryWithCycle;
import static com.bigcompany.common.ValidatorTestHelper.createRepositoryWithNoCycles;
import static com.bigcompany.exception.DataValidationException.ErrorCode.LOOP_FOUND_IN_ORG_STRUCTURE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CyclesValidatorTest {

    @Test
    public void shouldProperlyHandleCaseWithoutCyclesInOrgStructure() {
        //given
        CyclesValidator validator = new CyclesValidator();
        OrgStructureRepository repository = createRepositoryWithNoCycles();

        //when
        try {
            validator.validate(repository);
            //pass
        } catch (DataValidationException e) {
            fail("Should not throw any exception if there is no cycles in org structure");
        }
    }

    @Test
    public void shouldProperlyHandleCaseWithCycleInOrgStructure() {
        //given
        CyclesValidator validator = new CyclesValidator();
        OrgStructureRepository repository = createRepositoryWithCycle();

        //when
        try {
            validator.validate(repository);
            fail("Should throw exception if there is  cycle in org structure");
        } catch (DataValidationException e) {
            //pass
            assertEquals("Exception with following errorCode: LOOP_FOUND_IN_ORG_STRUCTURE should be thrown",
                    LOOP_FOUND_IN_ORG_STRUCTURE, e.getErrorCode());
        }
    }
}
