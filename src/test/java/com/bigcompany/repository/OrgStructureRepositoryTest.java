package com.bigcompany.repository;

import com.bigcompany.common.AbstractNotificationTest;
import com.bigcompany.model.Employee;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class OrgStructureRepositoryTest extends AbstractNotificationTest {

    @Test
    public void shouldReturnEmptyListsInCaseOfEmptyRepository() {
        //given
        OrgStructureRepository repository = new OrgStructureRepository();

        //when

        //then
        assertTrue("Should return empty list", repository.getAllEmployees().isEmpty());
        assertTrue("Should return empty set", repository.getManagersSubordinates().isEmpty());
    }

    @Test
    public void shouldProperlyBuildRepositoryOutOfAddedEmployees() {
        //given
        OrgStructureRepository repository = new OrgStructureRepository();

        //when
        Employee john = new Employee("1", "John", "Doe", 10000L, null);
        Employee ben = new Employee("2", "Ben", "Smith", 10000L, "1");
        Employee rob = new Employee("3", "Rob", "Novak", 10000L, "2");
        Employee karen = new Employee("4", "Karen", "Lamb", 10000L, "3");
        Employee alena = new Employee("5", "Alena", "White", 10000L, "3");
        Employee dana = new Employee("6", "Dana", "Diesel", 10000L, "3");
        repository.addEmployee(john);
        repository.addEmployee(ben);
        repository.addEmployee(rob);
        repository.addEmployee(karen);
        repository.addEmployee(alena);
        repository.addEmployee(dana);

        //then
        assertEquals("Incorrect number of elements in repository", 6, repository.getAllEmployees().size());
        assertEquals("Returned employee not equal to added one", john, repository.getEmployeeBy(john.getId()));
        assertEquals("Returned employee not equal to added one", karen, repository.getEmployeeBy(karen.getId()));
        assertEquals("Returned employee not equal to added one", dana, repository.getEmployeeBy(dana.getId()));
        Set<Map.Entry<String, List<Employee>>> subs = repository.getManagersSubordinates();
        assertEquals("Incorrect number of subordinates lists", 4, subs.size());
        for (Map.Entry<String, List<Employee>> sub : subs) {
            if (sub.getKey() == john.getId()) {
                assertEquals("Incorrect number of subordinates", 1, sub.getValue().size());
            }
            if (sub.getKey() == rob.getId()) {
                assertEquals("Incorrect number of subordinates", 3, sub.getValue().size());
            }
        }
    }
}
