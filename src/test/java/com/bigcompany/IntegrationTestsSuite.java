package com.bigcompany;

import com.bigcompany.service.OrganizationAnalysisServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        OrganizationAnalysisServiceTest.class
})
public class IntegrationTestsSuite {
}
