package com.bigcompany.analyser;

import com.bigcompany.common.AbstractNotificationTest;
import com.bigcompany.model.Employee;
import com.bigcompany.repository.OrgStructureRepository;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ReportingLinesAnalyserTest extends AbstractNotificationTest {


    @Test
    public void shouldReportCorrectMessageInCaseOfEmptyOrgStructure() {
        //given
        OrgStructureRepository org = new OrgStructureRepository();
        ReportingLinesAnalyser analyser = new ReportingLinesAnalyser(notificationService);

        //when
        analyser.analyse(org);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return only one message", 1, outputLines.length);
        assertEquals("There is no employees having more than 4 managers", outputLines[0]);
    }

    @Test
    public void shouldReportCorrectMessageInCaseOfNoEmployeesWithLessThan4Managers() {
        //given
        OrgStructureRepository org = new OrgStructureRepository();
        org.addEmployee(new Employee("1", "John", "Doe", 10000L, null));
        org.addEmployee(new Employee("2", "Ben", "Smith", 10000L, "1"));
        org.addEmployee(new Employee("3", "Rob", "Novak", 10000L, "2"));
        org.addEmployee(new Employee("4", "Karen", "Lamb", 10000L, "3"));
        org.addEmployee(new Employee("5", "Alena", "White", 10000L, "4"));
        org.addEmployee(new Employee("6", "Dana", "Diesel", 10000L, "5"));
        ReportingLinesAnalyser analyser = new ReportingLinesAnalyser(notificationService);

        //when
        analyser.analyse(org);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return only one message", 1, outputLines.length);
        assertEquals("There is no employees having more than 4 managers", outputLines[0]);
    }

    @Test
    public void shouldReportCorrectMessageInCaseOfSomeEmployeeHaveMoreThanFourManagersToCIO() {
        //given
        OrgStructureRepository org = new OrgStructureRepository();
        org.addEmployee(new Employee("1", "John", "Doe", 10000L, null));
        org.addEmployee(new Employee("2", "Ben", "Smith", 10000L, "1"));
        org.addEmployee(new Employee("3", "Rob", "Novak", 10000L, "2"));
        org.addEmployee(new Employee("4", "Karen", "Lamb", 10000L, "3"));
        org.addEmployee(new Employee("5", "Alena", "White", 10000L, "4"));
        org.addEmployee(new Employee("6", "Dana", "Diesel", 10000L, "5"));
        org.addEmployee(new Employee("7", "Bob", "Clarkson", 10000L, "6"));
        org.addEmployee(new Employee("8", "Emma", "Wats", 10000L, "7"));
        ReportingLinesAnalyser analyser = new ReportingLinesAnalyser(notificationService);

        //when
        analyser.analyse(org);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return only two messages", 2, outputLines.length);
        assertEquals("Bob Clarkson has a reporting line too long by 1", outputLines[0]);
        assertEquals("Emma Wats has a reporting line too long by 2", outputLines[1]);
    }
}
