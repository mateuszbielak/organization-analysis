package com.bigcompany.analyser;

import com.bigcompany.common.AbstractNotificationTest;
import com.bigcompany.model.Employee;
import com.bigcompany.repository.OrgStructureRepository;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SalaryAbstractAnalyserTest extends AbstractNotificationTest {


    @Test
    public void shouldReportCorrectMessageInCaseOfEmptyOrgStructure() {
        //given
        OrgStructureRepository org = new OrgStructureRepository();
        SalaryAnalyser analyser = new SalaryAnalyser(notificationService);

        //when
        analyser.analyse(org);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return only one message", 1, outputLines.length);
        assertEquals("There is no managers with incorrect salary", outputLines[0]);
    }

    @Test
    public void shouldReportCorrectMessageInCaseOfNoManagerHasToHighAndToLowSalary() {
        //given
        OrgStructureRepository org = new OrgStructureRepository();
        org.addEmployee(new Employee("1", "John", "Doe", 13000L, null));
        org.addEmployee(new Employee("2", "Ben", "Smith", 10000L, "1"));
        SalaryAnalyser analyser = new SalaryAnalyser(notificationService);

        //when
        analyser.analyse(org);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return only one message", 1, outputLines.length);
        assertEquals("There is no managers with incorrect salary", outputLines[0]);
    }

    @Test
    public void shouldReportCorrectMessageInCaseOfOneManagerHasToLowSalary() {
        //given
        OrgStructureRepository org = new OrgStructureRepository();
        org.addEmployee(new Employee("1", "John", "Doe", 14000L, null));
        org.addEmployee(new Employee("2", "Ben", "Smith", 10000L, "1"));
        org.addEmployee(new Employee("3", "Rob", "Novak", 12000L, "2"));
        org.addEmployee(new Employee("4", "Karen", "Lamb", 8000L, "2"));

        SalaryAnalyser analyser = new SalaryAnalyser(notificationService);

        //when
        analyser.analyse(org);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return only one message", 1, outputLines.length);
        assertEquals("Ben Smith earns less than they should by: 2000.00", outputLines[0]);
    }

    @Test
    public void shouldReportCorrectMessageInCaseOfOneManagerHasToHighSalary() {
        //given
        OrgStructureRepository org = new OrgStructureRepository();
        org.addEmployee(new Employee("1", "John", "Doe", 21000L, null));
        org.addEmployee(new Employee("2", "Ben", "Smith", 17000L, "1"));
        org.addEmployee(new Employee("3", "Rob", "Novak", 12000L, "2"));
        org.addEmployee(new Employee("4", "Karen", "Lamb", 8000L, "2"));

        SalaryAnalyser analyser = new SalaryAnalyser(notificationService);

        //when
        analyser.analyse(org);

        //then
        String[] outputLines = notifications.toString().trim().split(System.lineSeparator());
        assertEquals("Should return only one message", 1, outputLines.length);
        assertEquals("Ben Smith earns more than they should by: 2000.00", outputLines[0]);
    }
}
