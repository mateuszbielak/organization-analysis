package com.bigcompany.model;

import java.math.BigDecimal;
import java.util.Objects;

public final class Employee {
    private final String id;
    private final String name;
    private final String surname;
    private final BigDecimal salary;
    private final String managerId;

    public Employee(String id, String name, String surname, Long salary, String managerId) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.salary = new BigDecimal(salary);
        this.managerId = managerId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public String getManagerId() {
        return managerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) && Objects.equals(name, employee.name) && Objects.equals(surname, employee.surname) && Objects.equals(salary, employee.salary) && Objects.equals(managerId, employee.managerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, salary, managerId);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", salary=" + salary +
                ", managerId='" + managerId + '\'' +
                '}';
    }
}
