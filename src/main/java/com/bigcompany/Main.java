package com.bigcompany;

import com.bigcompany.service.NotificationService;
import com.bigcompany.service.OrganizationAnalysisService;

public class Main {
    public static void main(String[] args) {
        NotificationService notificationService = new NotificationService(System.out);
        if (args.length > 0) {
            notificationService.notifyAbout("Analysing organization from [" + args[0] + "] file.");

            try {
                OrganizationAnalysisService organizationAnalysisService = new OrganizationAnalysisService(notificationService);
                organizationAnalysisService.analyseOrganization(args[0]);
            } catch (Exception e) {
                notificationService.notifyAbout("An unexpected error occurred: " + e.getMessage());
            }
        } else {
            notificationService.notifyAbout("No org structure file path provided.");
        }
    }
}
