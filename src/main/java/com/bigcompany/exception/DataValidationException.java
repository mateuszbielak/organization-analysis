package com.bigcompany.exception;

public class DataValidationException extends RuntimeException {
    public enum ErrorCode {
        INVALID_ROOT_NODE_COUNT,
        NOT_EXISTING_MANAGER_ID,
        LOOP_FOUND_IN_ORG_STRUCTURE
    }

    private final DataValidationException.ErrorCode errorCode;

    public DataValidationException(DataValidationException.ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public DataValidationException.ErrorCode getErrorCode() {
        return errorCode;
    }
}
