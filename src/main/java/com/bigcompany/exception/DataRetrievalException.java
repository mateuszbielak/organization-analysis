package com.bigcompany.exception;

public class DataRetrievalException extends RuntimeException {
    public enum ErrorCode {
        FILE_OPEN_FAILED,
        EMPTY_CSV_FILE,
        TO_BIG_CSV_FILE,
        UNEXPECTED_COLUMN_COUNT,
        INVALID_COLUMN_FORMAT,
        UNKNOWN_ERROR
    }

    private final ErrorCode errorCode;

    public DataRetrievalException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
