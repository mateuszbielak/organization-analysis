package com.bigcompany.analyser;

import com.bigcompany.model.Employee;
import com.bigcompany.repository.OrgStructureRepository;
import com.bigcompany.service.NotificationService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.math.RoundingMode.HALF_UP;

public class SalaryAnalyser implements OrgStructureAnalyser {

    private static final String THERE_IS_NO_MANAGERS_WITH_INCORRECT_SALARY_RESPONSE = "There is no managers with incorrect salary";
    public static final String MAX_SALARY_MULTIPLIER = "1.5";
    public static final String MIN_SALARY_MULTIPLIER = "1.2";
    private static int SALARY_PRECISION = 2;
    private final NotificationService notificationService;

    public SalaryAnalyser(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public void analyse(OrgStructureRepository org) {
        Set<Map.Entry<String, List<Employee>>> managersSubordinates =
                org.getManagersSubordinates();

        if(managersSubordinates.isEmpty()) {
            this.notificationService.notifyAbout(THERE_IS_NO_MANAGERS_WITH_INCORRECT_SALARY_RESPONSE);
            return;
        }

        boolean hasAnyoneIncorrectSalary = false;

        for(Map.Entry<String, List<Employee>> entry : managersSubordinates) {
            Employee manager = org.getEmployeeBy(entry.getKey());
            if(manager == null) {
                continue;
            }

            List<Employee> subordinates = entry.getValue();
            if(subordinates.isEmpty()) {
                continue;
            }

            BigDecimal averageSalary = calculateAverageSalaryFor(subordinates);
            BigDecimal minSalary = calculateMinSalaryFor(averageSalary);
            BigDecimal maxSalary = calculateMaxSalaryFor(averageSalary);

            if(hasManagerToLowSalary(manager, minSalary)) {
                hasAnyoneIncorrectSalary = true;
                notificationService.notifyAbout(manager.getName() + " " + manager.getSurname() + " earns less than they should by: "
                        + minSalary.subtract(manager.getSalary()));
            }
            if(hasManagerToHighSalary(manager, maxSalary)) {
                hasAnyoneIncorrectSalary = true;
                notificationService.notifyAbout(manager.getName() + " " + manager.getSurname() + " earns more than they should by: "
                        + manager.getSalary().subtract(maxSalary));
            }
        }

        if(!hasAnyoneIncorrectSalary) {
            this.notificationService.notifyAbout(THERE_IS_NO_MANAGERS_WITH_INCORRECT_SALARY_RESPONSE);
        }
    }

    private static BigDecimal calculateMaxSalaryFor(BigDecimal averageSalary) {
        return averageSalary.multiply(new BigDecimal(MAX_SALARY_MULTIPLIER)).setScale(SALARY_PRECISION, HALF_UP);
    }

    private static BigDecimal calculateMinSalaryFor(BigDecimal averageSalary) {
        return averageSalary.multiply(new BigDecimal(MIN_SALARY_MULTIPLIER)).setScale(SALARY_PRECISION, HALF_UP);
    }

    private static BigDecimal calculateAverageSalaryFor(List<Employee> subs) {
        return subs.stream()
                .map(Employee::getSalary)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(subs.size()), SALARY_PRECISION, HALF_UP);
    }

    private static boolean hasManagerToHighSalary(Employee manager, BigDecimal maxSalary) {
        return manager.getSalary().compareTo(maxSalary) > 0;
    }

    private static boolean hasManagerToLowSalary(Employee manager, BigDecimal minSalary) {
        return manager.getSalary().compareTo(minSalary) < 0;
    }
}
