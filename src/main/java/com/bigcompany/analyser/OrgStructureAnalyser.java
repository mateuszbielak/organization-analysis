package com.bigcompany.analyser;

import com.bigcompany.repository.OrgStructureRepository;

public interface OrgStructureAnalyser {
    void analyse(OrgStructureRepository orgStructureRepository);
}
