package com.bigcompany.analyser;

import com.bigcompany.model.Employee;
import com.bigcompany.repository.OrgStructureRepository;
import com.bigcompany.service.NotificationService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ReportingLinesAnalyser implements OrgStructureAnalyser {

    private static final String NO_EMPLOYEES_WITH_TOO_MANY_MANAGERS_RESPONSE = "There is no employees having more than 4 managers";

    private final NotificationService notificationService;

    public ReportingLinesAnalyser(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @Override
    public void analyse(OrgStructureRepository org) {

        List<Employee> allEmployees = org.getAllEmployees();

        if (allEmployees.isEmpty()) {
            this.notificationService.notifyAbout(NO_EMPLOYEES_WITH_TOO_MANY_MANAGERS_RESPONSE);
            return;
        }

        boolean hasAnyoneToManyManagers = false;

        for (Employee employee : allEmployees) {
            int reportingLine = getReportingLineLengthFor(org, employee);
            if (reportingLine > 5) {
                hasAnyoneToManyManagers = true;
                this.notificationService.notifyAbout(employee.getName() + " " + employee.getSurname()
                        + " has a reporting line too long by " + (reportingLine - 5));
            }
        }

        if (!hasAnyoneToManyManagers) {
            this.notificationService.notifyAbout(NO_EMPLOYEES_WITH_TOO_MANY_MANAGERS_RESPONSE);
        }
    }

    private int getReportingLineLengthFor(OrgStructureRepository org, Employee employee) {
        int depth = 0;
        Employee currentEmployee = employee;
        Set<String> reportingLineIds = new HashSet<>();
        while (currentEmployee.getManagerId() != null && !currentEmployee.getManagerId().isEmpty()) {
            if (orgStructureCycleFound(reportingLineIds, currentEmployee)) break;
            depth++;
            currentEmployee = org.getEmployeeBy(currentEmployee.getManagerId());
        }
        return depth;
    }

    private static boolean orgStructureCycleFound(Set<String> reportingLineIds, Employee currentEmployee) {
        if (reportingLineIds.contains(currentEmployee.getManagerId())) {
            return true;
        } else {
            reportingLineIds.add(currentEmployee.getManagerId());
        }
        return false;
    }
}
