package com.bigcompany.repository;

import com.bigcompany.model.Employee;

import java.util.*;

/**
 * Maintains and manages organizational structure data, including employee details and
 * their hierarchical relationships.
 */
public class OrgStructureRepository {
    private final Map<String, Employee> employees = new HashMap<>();
    private final Map<String, List<Employee>> managersSubordinates = new HashMap<>();

    /**
     * Adds an employee to the repository and maps them under their manager.
     *
     * @param employee the employee to add
     */
    public void addEmployee(Employee employee) {
        employees.put(employee.getId(), employee);

        List<Employee> subs = managersSubordinates.computeIfAbsent(employee.getManagerId(), k -> new ArrayList<>());
        subs.add(employee);
    }

    /**
     * Retrieves an employee by their unique identifier.
     *
     * @param id the employee's ID
     * @return the employee if found, or null if not found
     */
    public Employee getEmployeeBy(String id) {
        return employees.get(id);
    }

    /**
     * Returns a list of all employees in the organization.
     *
     * @return a list of all employees
     */
    public List<Employee> getAllEmployees() {
        return new ArrayList<>(employees.values());
    }

    /**
     * Provides access to a set of entries mapping managers to their subordinates.
     *
     * @return a set of entries representing manager-subordinate relationships
     */
    public Set<Map.Entry<String, List<Employee>>> getManagersSubordinates() {
        return managersSubordinates.entrySet();
    }
}
