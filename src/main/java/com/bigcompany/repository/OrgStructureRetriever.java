package com.bigcompany.repository;

import com.bigcompany.exception.DataRetrievalException;
import com.bigcompany.model.Employee;
import com.bigcompany.repository.validator.CyclesValidator;
import com.bigcompany.repository.validator.ManagerIdValidator;
import com.bigcompany.repository.validator.RootNodeValidator;
import com.bigcompany.repository.validator.OrgStructureValidator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.bigcompany.exception.DataRetrievalException.ErrorCode.*;

/**
 * Responsible for retrieving and parsing organizational structure data from a specified file.
 * This class provides functionality to read organizational data formatted as CSV, ignoring
 * the header line and creating {@link Employee} objects for each subsequent line.
 * It also enforces structural integrity through several validators.
 */
public class OrgStructureRetriever {

    private static final String CSV_SEPARATOR = ",";
    private static final int MAX_LINES_NUMBER = 1000;
    public static final String COLUMN_NAMES_LINE_PREFIX = "id";

    private final List<OrgStructureValidator> orgStructureValidators = List.of(
            new RootNodeValidator(),
            new CyclesValidator(),
            new ManagerIdValidator()
    );

    public OrgStructureRetriever() {
    }

    /**
     * Reads organizational data from a CSV file, skips the header, and populates an {@link OrgStructureRepository}
     * with {@link Employee} entries based on the remaining lines.
     *
     * @param orgFilePath the path to the CSV file containing the organizational data
     * @return a populated {@link OrgStructureRepository}
     * @throws DataRetrievalException if the file cannot be read, is empty, exceeds allowed line counts,
     *                                or fails structural validation
     */
    public OrgStructureRepository retrieveFrom(String orgFilePath) {

        OrgStructureRepository repository = new OrgStructureRepository();

        List<String> allLines = readAllLinesFromFile(orgFilePath);

        if (allLines.isEmpty()) {
            throw new DataRetrievalException(EMPTY_CSV_FILE, "Provided CSV file is empty");
        }

        if (allLines.size() > MAX_LINES_NUMBER) {
            throw new DataRetrievalException(TO_BIG_CSV_FILE, "Provided CSV file contains too many lines: [" + allLines.size()
                    + "] while allowed only: [" + MAX_LINES_NUMBER + "]");
        }

        for (String line : allLines) {
            processLine(line, repository);
        }

        validateRepository(repository);

        return repository;
    }

    private void validateRepository(OrgStructureRepository org) {
        this.orgStructureValidators.forEach(validator -> validator.validate(org));
    }

    private void processLine(String line, OrgStructureRepository repository) {
        if (line.trim().isEmpty()) return;

        if (line.toLowerCase().startsWith(COLUMN_NAMES_LINE_PREFIX)) return;

        String[] parts = line.split(CSV_SEPARATOR, -1);

        if (parts.length < 4 || parts.length > 5) {
            throw new DataRetrievalException(UNEXPECTED_COLUMN_COUNT, "Incorrect number of columns in line: " + line);
        }

        long salary = retrieveSalaryFrom(parts, line);
        String managerId = retrieveManagerIdFrom(parts);

        Employee employee = new Employee(
                extractFieldFrom(parts[0], line),
                extractFieldFrom(parts[1], line),
                extractFieldFrom(parts[2], line),
                salary,
                managerId);

        addEmployeeIfNotExists(repository, employee);
    }

    private static void addEmployeeIfNotExists(OrgStructureRepository repository, Employee employee) {
        if (repository.getEmployeeBy(employee.getId()) == null) {
            repository.addEmployee(employee);
        } else {
            throw new DataRetrievalException(INVALID_COLUMN_FORMAT, "Multiple employees with given id: " + employee.getId());
        }
    }

    private String retrieveManagerIdFrom(String[] parts) {
        if (parts.length == 5) {
            String managerId = parts[4];
            if (managerId != null && !managerId.isBlank()) {
                return managerId;
            }
        }
        return null;
    }

    private static long retrieveSalaryFrom(String[] parts, String line) {
        try {
            return Long.parseLong(parts[3]);
        } catch (NumberFormatException e) {
            throw new DataRetrievalException(INVALID_COLUMN_FORMAT, "Invalid number format for salary in line: " + line);
        }
    }


    private String extractFieldFrom(String part, String line) {
        if (part == null || part.isBlank()) {
            throw new DataRetrievalException(INVALID_COLUMN_FORMAT, "One of the fields is empty in line: " + line);
        } else return part;
    }

    private List<String> readAllLinesFromFile(String orgFilePath) {
        try {
            return Files.readAllLines(Paths.get(orgFilePath));
        } catch (IOException e) {
            throw new DataRetrievalException(FILE_OPEN_FAILED, "Could not open following file: [" + orgFilePath + "]");
        }
    }
}
