package com.bigcompany.repository.validator;

import com.bigcompany.repository.OrgStructureRepository;

public interface OrgStructureValidator {
    void validate(OrgStructureRepository org);
}
