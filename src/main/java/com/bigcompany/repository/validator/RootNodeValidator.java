package com.bigcompany.repository.validator;

import com.bigcompany.exception.DataValidationException;
import com.bigcompany.model.Employee;
import com.bigcompany.repository.OrgStructureRepository;

import static com.bigcompany.exception.DataValidationException.ErrorCode.INVALID_ROOT_NODE_COUNT;

/**
 * Validates that the organizational structure contains exactly one root node, typically the CEO.
 * This ensures a clear and unambiguous point of leadership within the organization.
 */
public class RootNodeValidator implements OrgStructureValidator {

    /**
     * Validates the entire organization to ensure there is exactly one root node. A root node
     * is defined as an employee without a manager (i.e., the manager ID is null or blank).
     * This method counts such nodes and throws an exception if their number is not exactly one.
     *
     * @param org the {@link OrgStructureRepository} containing all organizational data.
     * @throws DataValidationException if there is not exactly one root node,
     *         indicating a structural problem in the organization hierarchy.
     */
    @Override
    public void validate(OrgStructureRepository org) {
        int rootNodeCount = 0;
        for (Employee employee : org.getAllEmployees()) {
            if (employee.getManagerId() == null || employee.getManagerId().isBlank()) {
                rootNodeCount++;
            }
        }
        if (rootNodeCount != 1) {
            throw new DataValidationException(INVALID_ROOT_NODE_COUNT, "Organisation hierarchy should contain exactly one root node (CEO)");
        }
    }
}
