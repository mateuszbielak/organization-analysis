package com.bigcompany.repository.validator;

import com.bigcompany.exception.DataValidationException;
import com.bigcompany.model.Employee;
import com.bigcompany.repository.OrgStructureRepository;

import static com.bigcompany.exception.DataValidationException.ErrorCode.NOT_EXISTING_MANAGER_ID;

/**
 * Validates that every employee's manager ID corresponds to an existing employee within
 * the organizational structure.
 */
public class ManagerIdValidator implements OrgStructureValidator {

    /**
     * Checks each employee in the organization to ensure their manager's ID points to
     * an existing employee. This validation helps maintain integrity in reporting lines.
     *
     * @param org the {@link OrgStructureRepository} containing all organizational data.
     * @throws DataValidationException if an employee's manager ID does not correspond
     *         to any existing employee, indicating a referencing error.
     */
    @Override
    public void validate(OrgStructureRepository org) {
        for (Employee employee : org.getAllEmployees()) {
            if (employee.getManagerId() != null && !employee.getManagerId().isBlank()) {
                Employee manager = org.getEmployeeBy(employee.getManagerId());
                if (manager == null) {
                    throw new DataValidationException(NOT_EXISTING_MANAGER_ID,
                            "Wrong manager id - there is no employee with given id: " + employee.getId());
                }
            }
        }
    }
}
