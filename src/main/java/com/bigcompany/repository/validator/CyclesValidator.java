package com.bigcompany.repository.validator;

import com.bigcompany.exception.DataValidationException;
import com.bigcompany.model.Employee;
import com.bigcompany.repository.OrgStructureRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.bigcompany.exception.DataValidationException.ErrorCode.LOOP_FOUND_IN_ORG_STRUCTURE;

/**
 * Validates the organizational structure to ensure that there are no cycles in the hierarchy.
 * A cycle occurs when an employee indirectly or directly reports to themselves through
 * the chain of management, which is an error in the structure.
 */
public class CyclesValidator implements OrgStructureValidator {
    /**
     * Iterates through each manager-subordinate pair in the organization to detect cycles.
     * Throws {@link DataValidationException} if a reporting loop is found, indicating a problem
     * in the structure.
     *
     * @param org the {@link OrgStructureRepository} to be validated.
     * @throws DataValidationException if a cycle is detected in the organizational structure.
     */
    @Override
    public void validate(OrgStructureRepository org) {
        for (Map.Entry<String, List<Employee>> managersSubordinate : org.getManagersSubordinates()) {
            List<Employee> subs = managersSubordinate.getValue();
            subs.forEach(employee -> {
                Set<String> reportingLineIds = new HashSet<>();
                Employee currentEmployee = employee;
                while (currentEmployee.getManagerId() != null && !currentEmployee.getManagerId().isEmpty()) {
                    if (orgStructureCycleFound(reportingLineIds, currentEmployee)) {
                        throw new DataValidationException(LOOP_FOUND_IN_ORG_STRUCTURE, "There is a loop in organisation structure " + subs);
                    }
                    currentEmployee = org.getEmployeeBy(currentEmployee.getManagerId());
                }
            });
        }
    }

    private static boolean orgStructureCycleFound(Set<String> reportingLineIds, Employee currentEmployee) {
        if (reportingLineIds.contains(currentEmployee.getManagerId())) {
            return true;
        } else {
            reportingLineIds.add(currentEmployee.getManagerId());
        }
        return false;
    }
}
