package com.bigcompany.service;

import com.bigcompany.analyser.OrgStructureAnalyser;
import com.bigcompany.analyser.ReportingLinesAnalyser;
import com.bigcompany.analyser.SalaryAnalyser;
import com.bigcompany.exception.DataRetrievalException;
import com.bigcompany.exception.DataValidationException;
import com.bigcompany.repository.OrgStructureRepository;
import com.bigcompany.repository.OrgStructureRetriever;

import java.io.IOException;
import java.util.List;


/**
 * Provides service for analyzing the organizational structure by utilizing various
 * analyzers and retrieving organizational data. This service supports multiple types of
 * analysis, including salary and reporting lines, through a list of {@link OrgStructureAnalyser}.
 */
public class OrganizationAnalysisService {

    private final OrgStructureRetriever orgStructureRetriever;
    private final List<OrgStructureAnalyser> orgStructureAnalysers;
    private final NotificationService notificationService;

    /**
     * Constructs an {@link OrganizationAnalysisService} with a given {@link NotificationService} and initializes
     * the {@link OrgStructureRetriever} along with predefined {@link OrgStructureAnalyser}s.
     *
     * <p>The analyzers included by default are:
     * <ul>
     *     <li>{@link SalaryAnalyser} - Analyzes employee salary data.</li>
     *     <li>{@link ReportingLinesAnalyser} - Analyzes reporting line structures within the organization.</li>
     * </ul>
     */
    public OrganizationAnalysisService(NotificationService notificationService) {
        this.notificationService = notificationService;
        this.orgStructureRetriever = new OrgStructureRetriever();
        this.orgStructureAnalysers = List.of(
                new SalaryAnalyser(notificationService),
                new ReportingLinesAnalyser(notificationService)
        );
    }

    /**
     * Analyzes the organizational structure from a specified file path. This method retrieves
     * the organization structure data from the given path and applies all configured analyses.
     *
     * @param orgFilePath the file path where the organization structure data is stored.
     */
    public void analyseOrganization(String orgFilePath) {
        try {
            OrgStructureRepository org = orgStructureRetriever.retrieveFrom(orgFilePath);
            if (org != null) {
                orgStructureAnalysers.forEach(analyser -> analyser.analyse(org));
            }
        } catch (DataRetrievalException | DataValidationException e) {
            this.notificationService.notifyAbout(e.getMessage());
        }
    }
}
