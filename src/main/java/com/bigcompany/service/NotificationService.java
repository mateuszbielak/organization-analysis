package com.bigcompany.service;

import java.io.PrintStream;

/**
 * Handles sending notifications by printing messages to a specified {@link PrintStream}.
 */
public class NotificationService {

    private PrintStream out;

    /**
     * Constructs a NotificationService with a designated output stream.
     *
     * @param out the {@link PrintStream} to which notifications will be printed
     */
    public NotificationService(PrintStream out) {
        this.out = out;
    }

    /**
     * Sends a notification by printing a message to the configured {@link PrintStream}.
     *
     * @param message the message to be notified about
     */
    public void notifyAbout(String message) {
        this.out.println(message);
    }
}

