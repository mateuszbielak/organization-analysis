# ORGANIZATION ANALYSIS

## Description

The Organization Analysis Service is a Java application designed to manage and analyze organizational structures. 

This application facilitates the processing of employee data from CSV files, supporting the calculation and analysis of hierarchical employee relationships and salaries. 


## Getting Started

### Dependencies

#### Prerequisites
- **Java JDK 22**: Required for compiling and running the application.
- **JUnit 4.13.2**: Necessary for testing. Included under the test scope in Maven, not required for production.

#### Maven Configuration
- Dependencies are managed through Maven and should be automatically resolved when building the project.
- Maven Central Repository is used for fetching dependencies.

Ensure that Java JDK 22 is installed and that Maven is configured on your system before proceeding with the installation and execution of the application.

### Installing

To install this project, follow these steps:

1. **Clone the repository:**
    ```bash
   git clone https://gitlab.com/mateuszbielak/organization-analysis.git
    ```
2. **Navigate to the project directory:**
    ```bash
   cd organization-analysis
    ```
3. **Build the project with Maven:**
    ```bash
   mvn clean install
    ```
4. **Start the app with java:**
    ```bash
   java -jar .\target\organization-analysis-1.0-SNAPSHOT.jar path_to_org_csv_file.csv
    ```

### Running tests
```bash
mvn clean install
```

### Project Assumptions:
- assumed there is a need to log to console in case of:
  - starting of the application with given path
  - finding no issues in salary analysis
  - finding no issues in reporting lines analysis
  - starting app without giving a file path
  - starting app with incorrect file path
  - specific error message in case of data validation issues
- only "," is allowed as CSV columns separator
- order of lines in CSV file is not important
- cycles are handled as incorrect input
- not a number salary is handled as incorrect input
- multiple root nodes are handled as incorrect input
- empty fields are handled as incorrect input
- not unique id's for employees are handled as incorrect input
- not existing manager id's are handled as incorrect input
- salaries are represented as integer values, without decimal points
- the precision for calculation is set to two decimal points, using the 'half-up' rounding method
- the first row may contain column titles, starting with 'Id', although this is not mandatory
- when a CSV file contains more than 1,000 lines, it is classified as invalid, and an error message is logged accordingly
- when a CSV file is empty, it is classified as invalid, and an error message is logged accordingly
- empty lines are skipped
- in case of any validation errors in particular line we stop processing with the info as issue with parsing one line may impact the whole report
